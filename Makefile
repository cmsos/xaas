#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2021, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Project level Makefile
#
##

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES= \
    template \
    slim/minidaq/zone \
    slim/minidaq/client \
    slim/minidaq/service \
    slim/minidaq/settings \
    slim/minidaq/extension \
    slim/minidaq/addon \
    slim/hilton/zone \
    slim/hilton/client \
    slim/hilton/service \
    slim/hilton/settings \
    slim/hilton/extension \
    slim/dqm/service \
    slim/dqm/addon \
    slim/dqm/zone \
    slim/dqm/client \
    slim/dqm/settings \
    slim/dqm/extension \
    slim/lab40/zone \
    slim/lab40/client \
    slim/lab40/service \
    slim/lab40/settings \
    slim/cdaq904/zone \
    slim/cdaq904/client \
    slim/cdaq904/service \
    slim/cdaq904/settings \
    open/cdaq/zone \
    open/cdaq/client \
    open/cdaq/service \
    open/cdaq/settings \
    open/daqval/zone \
    open/daqval/client \
    open/daqval/service \
    open/daqval/settings \
    open/base/zone \
    open/base/client \
    slim/scal/service \
    slim/scal/addon \
    slim/scal/zone \
    slim/scal/client \
    slim/scal/settings \
    slim/scal/extension \
    slim/ecal/service \
    slim/ecal/addon \
    slim/ecal/zone \
    slim/ecal/client \
    slim/ecal/settings \
    slim/ecal/extension \
    slim/ecal904/service \
    slim/ecal904/zone \
    slim/ecal904/client \
    slim/ecal904/settings \
    slim/hcal904/service \
    slim/hcal904/zone \
    slim/hcal904/client \
    slim/hcal904/settings \
    slim/hcal904/addon \
    slim/hcal904/extension \
    slim/es/service \
    slim/es/addon \
    slim/es/zone \
    slim/es/client \
    slim/es/settings \
    slim/es/extension \
    slim/pixel/service \
    slim/pixel/addon \
    slim/pixel/zone \
    slim/pixel/client \
    slim/pixel/settings \
    slim/pixel/extension \
    slim/tracker/service \
    slim/tracker/addon \
    slim/tracker/zone \
    slim/tracker/client \
    slim/tracker/settings \
    slim/tracker/extension \
    slim/emu/service \
    slim/emu/addon \
    slim/emu/zone \
    slim/emu/client \
    slim/emu/settings \
    slim/emu/extension \
    open/cms/zone \
    open/cms/settings \
    open/cms/service \
    open/cms904/zone \
    open/cms904/settings \
    open/cms904/service \
    slim/daqtest/zone \
    slim/daqtest/client \
    slim/daqtest/service \
    slim/daqtest/settings \
    slim/daqtest/extension \
    slim/bril/service \
    slim/bril/addon \
    slim/bril/zone \
    slim/bril/client \
    slim/bril/settings \
    slim/bril/extension \
    slim/rpc/service \
    slim/rpc/addon \
    slim/rpc/zone \
    slim/rpc/client \
    slim/rpc/settings \
    slim/rpc/extension \
    slim/muondt/service \
    slim/muondt/addon \
    slim/muondt/zone \
    slim/muondt/client \
    slim/muondt/settings \
    slim/muondt/extension \
    slim/l1test/service \
    slim/l1test/zone \
    slim/l1test/client \
    slim/l1test/addon \
    slim/l1test/settings \
    slim/l1test/extension \
    slim/hcal/service \
    slim/hcal/addon \
    slim/hcal/zone \
    slim/hcal/client \
    slim/hcal/settings \
    slim/hcal/extension \
    slim/ctpps/service \
    slim/ctpps/addon \
    slim/ctpps/zone \
    slim/ctpps/client \
    slim/ctpps/settings \
    slim/ctpps/extension \
    slim/totem/service \
    slim/totem/addon \
    slim/totem/zone \
    slim/totem/client \
    slim/totem/settings \
    slim/totem/extension \
    slim/trigger/service \
    slim/trigger/addon \
    slim/trigger/zone \
    slim/trigger/client \
    slim/trigger/settings \
    slim/trigger/extension \
    slim/development/service \
    slim/development/addon \
    slim/development/zone \
    slim/development/client \
    slim/development/settings \
    slim/development/extension \
    slim/daq3val/service \
    slim/daq3val/addon \
    slim/daq3val/zone \
    slim/daq3val/client \
    slim/daq3val/settings \
    slim/cdaq3/service \
    slim/cdaq3/addon \
    slim/cdaq3/zone \
    slim/cdaq3/client \
    slim/cdaq3/settings \
    slim/mtd/service \
    slim/mtd/addon \
    slim/mtd/zone \
    slim/mtd/client \
    slim/mtd/settings \
    slim/mtd/extension \
    slim/zdc/service \
    slim/zdc/addon \
    slim/zdc/zone \
    slim/zdc/client \
    slim/zdc/settings \
    slim/zdc/extension \
    slim/crack/addon \
    slim/crack/zone
endif
export PACKAGES

BUILD_SUPPORT=build
export BUILD_SUPPORT

PROJECT_NAME=xaas
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
