extension:
  application:
    brilbus:
      name: brilbus
      externalport: __BRILBUSEXTERNALPORT
    spotlightocci:
      name: spotlightocci
      externalport: __SPOTLIGHTOCCIEXTERNALPORT
    heartbeat:
      name: heartbeat
      externalport: __HEARTBEATEXTERNALPORT
