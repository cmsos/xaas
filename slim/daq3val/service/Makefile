#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2024, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Makefile example for Slim Service 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

BUILD_SUPPORT=build
PROJECT_NAME=xaas

ifndef XAAS_ROOT
XAAS_ROOT=$(XDAQ_ROOT)
endif
export XAAS_ROOT

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
PackageName=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-1]}')
Package=slim/$(PackageName)

ifndef BUILD_VERSION
BUILD_VERSION=1
endif

PACKAGE_VER_MAJOR=1
PACKAGE_VER_MINOR=10
PACKAGE_VER_PATCH=0

TEMPLATEDIR=$(XAAS_ROOT)/template/slim/service

build: _buildall

_buildall: all

_all: all

default: all

all: 
	$(MAKE) -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(PackageName)
_installall: install

install: 

_cleanall: clean

clean:
	$(MAKE) -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(PackageName) clean

_packageall: package

package:
	$(MAKE) -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(PackageName) BUILD_VERSION=$(BUILD_VERSION) PACKAGE_VER_MAJOR=$(PACKAGE_VER_MAJOR) PACKAGE_VER_MINOR=$(PACKAGE_VER_MINOR) PACKAGE_VER_PATCH=$(PACKAGE_VER_PATCH) package


_installpackageall: installpackage

installpackage:
	$(MAKE) -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(PackageName) installpackage

_cleanpackageall: cleanpackage

cleanpackage:
	$(MAKE) -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(PackageName) cleanpackage


