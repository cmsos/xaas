#!/bin/bash

cp chart/*.tgz /var/www/html/chart/
helm repo index /var/www/html/chart/

helm repo update
helm install development cmsos/cmsos-xaas-helm-development

