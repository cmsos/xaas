#!/bin/bash

export KUBECONFIG=~/.kube/config

kubectl create -f namespace/namespace.yaml

kubectl create -n development configmap sensor --from-file=/usr/repos/xaas/slim/development/k8s/sensor/
kubectl create -n development configmap flash --from-file=/usr/repos/xaas/slim/development/k8s/flash/
kubectl create -n development configmap store --from-file=/usr/repos/xaas/slim/development/k8s/store/

kubectl create -n development configmap jobcontrol --from-file=/usr/repos/xaas/slim/development/k8s/profile/jobcontrol.profile
kubectl create -f service/jobcontrol.yaml
kubectl create -f daemonset/jobcontrol.yaml

kubectl create -n development configmap xaad --from-file=/usr/repos/xaas/slim/development/k8s/profile/xaad.profile
kubectl create -f service/xaad.yaml
kubectl create -f statefulset/xaad.yaml

kubectl create -n development configmap xmas-slash2g --from-file=/usr/repos/xaas/slim/development/k8s/profile/xmas-slash2g.profile
kubectl create -f service/xmas-slash2g.yaml
kubectl create -f deployment/xmas-slash2g.yaml

kubectl create -n development configmap spotlight2g --from-file=/usr/repos/xaas/slim/development/k8s/profile/spotlight2g.profile
kubectl create -f service/spotlight2g.yaml
kubectl create -f deployment/spotlight2g.yaml

kubectl create -n development configmap spotlightocci --from-file=/usr/repos/xaas/slim/development/k8s/profile/spotlightocci.profile
kubectl create -n development configmap jel --from-file=/usr/repos/xaas/slim/development/k8s/etc/jel.xml
kubectl create -n development configmap spotlightocl --from-file=/nfshome0/daqpro/cmsos/etc/spotlight.ocl
kubectl create -f service/spotlightocci.yaml
kubectl create -f deployment/spotlightocci.yaml

kubectl create -n development configmap tstore --from-file=/usr/repos/xaas/slim/development/k8s/profile/tstore.profile
kubectl create -f service/tstore.yaml
kubectl create -f deployment/tstore.yaml

kubectl create -n development configmap xmas-store --from-file=/usr/repos/xaas/slim/development/k8s/profile/xmas-store.profile
kubectl create -n development configmap xmas-storeocl --from-file=/nfshome0/daqpro/cmsos/etc/xmasstore.ocl
kubectl create -f service/xmas-store.yaml
kubectl create -f deployment/xmas-store.yaml

kubectl create -n development configmap psx --from-file=/usr/repos/xaas/slim/development/k8s/profile/psx.profile
kubectl create -f service/psx.yaml
kubectl create -f deployment/psx.yaml

kubectl create -n development configmap slash2g --from-file=/usr/repos/xaas/slim/development/k8s/profile/slash2g.profile
kubectl create -f service/slash2g.yaml
kubectl create -f statefulset/slash2g.yaml

kubectl create -n development configmap localbus --from-file=/usr/repos/xaas/slim/development/k8s/profile/localbus.profile
kubectl create -f service/localbus.yaml 
kubectl create -f statefulset/localbus.yaml

kubectl create -n development configmap slimbus --from-file=/usr/repos/xaas/slim/development/k8s/profile/slimbus.profile
kubectl create -f service/slimbus.yaml
kubectl create -f statefulset/slimbus.yaml

