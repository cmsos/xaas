import threadedcollector

def main():
  servers = [{"url": "kvm-s3562-1-ip129-03.cms:9741", "url": "kvm-s3562-1-ip129-03.cms:9742", "url": "kvm-s3562-1-ip129-04.cms:9743", "url": "kvm-s3562-1-ip129-04.cms:9744", "url": "kvm-s3562-1-ip129-05.cms:9745", "url": "kvm-s3562-1-ip129-05.cms:9746", "url": "kvm-s3562-1-ip129-06.cms:9747", "url": "kvm-s3562-1-ip129-06.cms:9748"}]
  outputdir = "/var/www/html/"
  formats = [{"format": "json", "option": ""}]
  threadedcollector.collect(servers, outputdir, formats)

if __name__ == "__main__":
  main()
