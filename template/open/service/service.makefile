# $Id: Makefile,v 1.110 2009/05/29 13:15:07 rmoser Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=$(ZONE_NAME)-service
Project=$(PROJECT_NAME)
Package=open/$(ZONE_NAME)/service

Summary=XAAS configuration for $(ZONE_NAME) service

Description=This package provides XAAS configuration for service on zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template/open
XAAS_SERVICE_HOST=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep XAAS_MACRO_BRIDGE_SERVICE_HOST | awk -F' ' '{print $$2}')
XAAS_BRIDGE2G_SENTINEL_PORT=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep XAAS_MACRO_BRIDGE2G_SENTINEL_HTTP_PORT | awk -F' ' '{print $$2}')

_all: all

default: all

all: clean
	mkdir -p system
	mkdir -p system-preset
	if [ -d $(BUILD_HOME)/$(Package)/system.template/ ]; then \
		find $(BUILD_HOME)/$(Package)/system.template/ -type f -name "*.template" -exec bash -c 'filename={}; filename=$$(basename -s .template $$filename); servicename=$${filename%.*}; servicetype=$${filename##*.}; cpp -P -I$(BUILD_HOME)/$(Package)/.. -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) -DXAAS_MACRO_UNIT_NAME=$${servicename} {} system/$(ZONE_NAME).$${servicename}@.$${servicetype};' \; ; \
	fi
	if [ -d $(BUILD_HOME)/$(Package)/system-preset.template/ ]; then \
		find $(BUILD_HOME)/$(Package)/system-preset.template/ -type f -name "*.template" -exec bash -c 'filename={}; filename=$$(basename -s .template $$filename); servicename=$${filename%.*}; cpp -P -I$(BUILD_HOME)/$(Package)/.. -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} system-preset/55-$(ZONE_NAME).service.$${servicename}.preset;' \; ; \
	fi
	mkdir xplore
	mkdir -p scope
	mkdir -p conf
	mkdir -p profile
	echo "service"  > scope/service.scope
	cp $(TEMPLATEDIR)/service/spec.template .
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	find $(TEMPLATEDIR)/service/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename%.template}' \;
	find $(TEMPLATEDIR)/service/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} conf/$${filename%.template}' \;
	find $(TEMPLATEDIR)/service/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cname=$${filename%.template}; unit=$${cname%.conf}; compopt=$$(echo $${unit} | sed 's/-/_/g'); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -D_$${compopt}_ -DXAAS_MACRO_UNIT_NAME=$${unit}  -DXAAS_MACRO_ENVIRNOMENTFILE=$${unit}".env" -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/system/serviceI.template system/$(ZONE_NAME).$${unit}@.service;' \;
	find conf -type f -name "*.conf" -exec bash -c 'export XDAQ_ZONE=$(ZONE_NAME); filename={}; filename=$$(basename $$filename); cname=$${filename%.template}; unit=$${cname%.conf}; export CONFIGURATION=$${unit}; set -a; source conf/$${filename%.template}; set +a; envsubst < conf/$${filename%.template} > conf/$${unit}.env' \;
	sed -i 's/@@/\/\//g' profile/*.profile 
	sed -i 's/@@/\/\//g' conf/*.conf
	sed -i 's/@@/\/\//g' conf/*.env
	$(XAAS_ROOT)/template/scripts/instantiateXML.awk "argument=conf/xdaqd.service.conf.in" hosts.setup
	$(XAAS_ROOT)/template/scripts/instantiateTargets.awk "templatedir=$(TEMPLATEDIR)/service/system" "targetdir=system" "zone=$(ZONE_NAME)" "scope=service" hosts.setup
	find system -type f -name "*.target.cpp" -exec bash -c 'filename={}; filename=$$(basename $$filename);  cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} system/$${filename%.cpp}' \;
	sed -i 's/@@/\/\//g' system/*.target
	sed -i 's/@@/\/\//g' system/*.service
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template/open -DXAAS_MACRO_ZONE=$(ZONE_NAME)  $(TEMPLATEDIR)/service/system-preset/preset.template system-preset/50-$(ZONE_NAME).service.preset
	
	

_cleanall: clean

clean:
	-rm -rf scope
	-rm -f spec.template 
	-rm -rf xplore 
	-rm -f conf/xdaqd.*.service.conf 
	-rm -rf cron.d
	-rm -rf conf/*.conf
	-rm -rf conf/*.env
	-rm -rf profile/*.profile
	-rm -rf system/*.service
	-rm -rf system/*.target
	-rm -rf system/*.cpp
	-rm -rf system-preset/*.preset
	

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
