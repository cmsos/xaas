# $Id

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2018, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=$(ZONE_NAME)-client
Project=$(PROJECT_NAME)
Package=open/$(ZONE_NAME)/client

Summary=XAAS configuration for $(ZONE_NAME) client

Description=This package provides XAAS configuration for client on zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template/open
XAAS_SERVICE_HOST=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/client/defaultvar.macros | grep XAAS_MACRO_BRIDGE_SERVICE_HOST | awk -F' ' '{print $$2}')
XAAS_BRIDGE2G_SENTINEL_PORT=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/client/defaultvar.macros | grep XAAS_MACRO_BRIDGE2G_SENTINEL_HTTP_PORT | awk -F'\t' '{print $$2}')

_all: all

default: all

all: clean
	mkdir xplore
	mkdir -p scope
	mkdir -p conf
	mkdir -p profile
	echo "client"  > scope/client.scope
	cp $(TEMPLATEDIR)/client/spec.template .
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	find $(TEMPLATEDIR)/base/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename%.template}' \;
	find $(TEMPLATEDIR)/base/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} conf/$${filename%.template}' \;
	sed -i 's/@@/\/\//g' profile/*.profile 
	sed -i 's/@@/\/\//g' conf/*.conf

_cleanall: clean

clean:
	-rm -rf scope
	-rm -rf xplore
	-rm -f spec.template 
	-rm -rf cron.d
	-rm -rf conf/*.conf
	-rm -rf profile/*.profile

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
