# $Id

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2019, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=$(ZONE_NAME)-zone
Project=$(PROJECT_NAME)
Package=$(PACKAGE_TYPE)/$(ZONE_NAME)/zone


Summary= XAAS configuration for zone $(ZONE_NAME)

Description= This package provides XAAS configuration for zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template

_all: all

default: all

all: clean
	mkdir -p etc/xdaq.d
	mkdir -p etc/logrotate.d
	mkdir -p etc/cron.d
	echo $(ZONE_NAME)  >  etc/xdaq.d/$(ZONE_NAME).zone
	cp $(XAAS_ROOT)/template/zone/spec.template .
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	cp $(XAAS_ROOT)/template/zone/cmsos.logrotate.template etc/logrotate.d/cmsos.$(ZONE_NAME)
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#g' etc/logrotate.d/cmsos.$(ZONE_NAME) 
	cp $(XAAS_ROOT)/template/zone/cmsos.cron.template etc/cron.d/cmsos.$(ZONE_NAME).cron
	chmod 0644 etc/cron.d/cmsos.$(ZONE_NAME).cron
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#g' etc/cron.d/cmsos.$(ZONE_NAME).cron
	mkdir -p system
	mkdir -p system-preset
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template/$(PACKAGE_TYPE) -DXAAS_MACRO_ZONE=$(ZONE_NAME)  $(TEMPLATEDIR)/zone/system/target.template system/$(ZONE_NAME).target
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template/$(PACKAGE_TYPE) -DXAAS_MACRO_ZONE=$(ZONE_NAME)  $(TEMPLATEDIR)/zone/system/notifyI.template system/$(ZONE_NAME).notify@.service
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template/$(PACKAGE_TYPE) -DXAAS_MACRO_ZONE=$(ZONE_NAME)  $(TEMPLATEDIR)/zone/system-preset/preset.template system-preset/50-$(ZONE_NAME).preset
	sed -i 's/@@/\/\//g' system/*.target
	sed -i 's/@@/\/\//g' system/*.service
	

_cleanall: clean

clean:
	-rm -rf etc 
	-rm -rf system
	-rm -rf system-preset
	-rm -f spec.template 
	

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
