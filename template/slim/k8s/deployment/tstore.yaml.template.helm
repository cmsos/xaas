{{- if .Values.zone.application.tstore.enabled }}
{{- $application := .Values.zone.application.tstore.name -}}
{{- $profilename := .Values.zone.application.tstore.profile -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $application }}
  namespace: {{ .Values.zone.name }}
spec:
  replicas: 1
  selector:
    matchLabels:
      application: {{ $application }}
  template:
    metadata:
      labels:
        application: {{ $application }}
    spec:
      hostname: {{ $application }}
      subdomain: {{ $application }}
      containers:
      - name: {{ $application }}
        image: {{ .Values.zone.dockerimage }}
        imagePullPolicy: Always
        ports:
        - containerPort: {{ .Values.zone.network.hyperdaq.port }}
        - containerPort: {{ .Values.zone.network.localnet.port }}
        - containerPort: {{ .Values.zone.network.slimnet.port }}
        readinessProbe:
        {{- include "readiness.probe" . | nindent 10 }}
        env:
        {{- include "application.env" . | nindent 8 }}
        command: ["/bin/bash", "-c"]
        args:
        - profile=$(mktemp);
          envsubst < /opt/xdaq/share/{{ .Values.zone.name }}/profile/{{ $profilename }}.profile > $profile;
        {{- include "localbus.ready" . | nindent 10 }}
        {{- include "slimbus.ready" . | nindent 10 }}
        {{- include "application.args" . | nindent 10 }}
        volumeMounts:
        - name: profile-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/profile
        - name: sensor-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/sensor
        - name: flash-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/flash
      volumes:
      - name: profile-volume
        configMap:
          name: {{ $application }}
      - name: sensor-volume
        configMap:
          name: sensor
      - name: flash-volume
        configMap:
          name: flash
{{- end }}
