include(`config.m4')dnl
{{- define "application.env" -}}
- name: XDAQ_ROOT
  value: /opt/xdaq
- name: LD_LIBRARY_PATH
  value: /opt/xdaq/lib:/opt/WinCC_OA/3.16/bin:/opt/WinCC_OA/3.16/api/lib.linux_RHEL4
- name: XDAQ_DOCUMENT_ROOT
  value: /opt/xdaq/htdocs
- name: XDAQ_SETUP_ROOT
  value: /opt/xdaq/share
- name: XDAQ_ZONE
  value: {{ .Values.zone.name }}
- name: POD_IP
  valueFrom:
    fieldRef:
      fieldPath: status.podIP
{{- end -}}

{{- define "application.args" -}}
/opt/xdaq/bin/xdaq.exe -p {{ .Values.zone.network.hyperdaq.port }} -e $profile -z {{ .Values.zone.name }}
{{- end -}}

{{- define "localbus.ready" -}}
for (( c=0; c<{{ .Values.zone.application.localbus.replicas }}; c++ ));
do
until nslookup localbus-$c.localbus;
do echo Waiting for the name localbus-$c.localbus to be resolved...;
sleep 10;
done;
done;
sleep 5;
{{- end -}}

{{- define "slimbus.ready" -}}
for (( c=0; c<{{ .Values.zone.application.slimbus.replicas }}; c++ ));
do
until nslookup slimbus-$c.slimbus;
do echo Waiting for the name slimbus-$c.slimbus to be resolved...;
sleep 10;
done;
done;
sleep 5;
{{- end -}}

{{- define "readiness.probe" -}}
tcpSocket:
  port: {{ .Values.zone.network.hyperdaq.port }}
initialDelaySeconds: 5
periodSeconds: 10
{{- end -}}
