{{- if .Values.zone.application.psx.enabled }}
{{- $application := .Values.zone.application.psx.name -}}
{{- $profilename := .Values.zone.application.psx.profile -}}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ $application }}
  namespace: {{ .Values.zone.name }}
spec:
  replicas: {{ .Values.zone.application.psx.replicas }}
  serviceName: {{ $application }}
  selector:
    matchLabels:
      application: {{ $application }}
  template:
    metadata:
      labels:
        application: {{ $application }}
    spec:
      hostname: {{ $application }}
      subdomain: {{ $application }}
      containers:
      - name: {{ $application }}
        image: {{ .Values.zone.dockerimage }}
        imagePullPolicy: Always
        ports:
        - containerPort: {{ .Values.zone.network.hyperdaq.port }}
        - containerPort: {{ .Values.zone.network.localnet.port }}
        - containerPort: {{ .Values.zone.network.slimnet.port }}
        readinessProbe:
        {{- include "readiness.probe" . | nindent 10 }}
        env:
        {{- include "application.env" . | nindent 8 }}
        - name: PVSS_II_HOME
          value: /opt/WinCC_OA/3.16
        - name: API_ROOT
          value: /opt/WinCC_OA/3.16/api
        - name: PLATFORM
          value: linux_RHEL4
        - name: PSXMANAGERNUMBERS
          valueFrom:
            configMapKeyRef:
              name: psxconfig
              key: psxmanagernumbers
        - name: PSXDATABASEMANAGERS
          valueFrom:
            configMapKeyRef:
              name: psxconfig
              key: psxdatabasemanagers
        - name: PSXEVENTMANAGERS
          valueFrom:
            configMapKeyRef:
              name: psxconfig
              key: psxeventmanagers
        - name: PSXDNSS
          valueFrom:
            configMapKeyRef:
              name: psxconfig
              key: psxdnss
        command: ["/bin/bash", "-c"]
        args:
        - profile=$(mktemp);
          export INSTANCE=${HOSTNAME#{{ $profilename }}-};
          array=($PSXMANAGERNUMBERS);
          export PSXMANAGERNUMBER=${array[$INSTANCE]};
          array=($PSXDATABASEMANAGERS);
          export PSXDATABASEMANAGER=${array[$INSTANCE]};
          array=($PSXEVENTMANAGERS);
          export PSXEVENTMANAGER=${array[$INSTANCE]};
          array=($PSXDNSS);
          export PSXDNS=${array[$INSTANCE]};          
          envsubst < /opt/xdaq/share/{{ .Values.zone.name }}/profile/{{ $profilename }}.profile > $profile;
          echo "$POD_IP $HOSTNAME.{{ $profilename }}" >> /etc/hosts;
        {{- include "localbus.ready" . | nindent 10 }}
        {{- include "slimbus.ready" . | nindent 10 }}
        {{- include "application.args" . | nindent 10 }}
        volumeMounts:
        - name: profile-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/profile
        - name: sensor-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/sensor
        - name: flash-volume
          mountPath: /opt/xdaq/share/{{ .Values.zone.name }}/flash
        - name: liveness-volume
          mountPath: /opt/scripts
        livenessProbe:
          exec:
            command:
              - /opt/scripts/liveness
              - "_Event.License.ExpirationCnt:_original.._value"
              - "http://{{ $application }}:{{ .Values.zone.network.hyperdaq.port }}"
          initialDelaySeconds: 120
          periodSeconds: 120
          successThreshold: 1
          failureThreshold: 1
          timeoutSeconds: 10
      volumes:
      - name: profile-volume
        configMap:
          name: {{ $application }}
      - name: sensor-volume
        configMap:
          name: sensor
      - name: flash-volume
        configMap:
          name: flash
      - name: liveness-volume
        configMap:
          name: psxliveness
          defaultMode: 0755
{{- end }}
