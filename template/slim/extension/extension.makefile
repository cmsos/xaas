#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2022, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=$(ZONE_NAME)-extension
Project=$(PROJECT_NAME)
Package=$(PACKAGE_TYPE)/$(ZONE_NAME)/extension

Summary=XAAS configuration for $(ZONE_NAME) service

Description=This package provides XAAS configuration for service on zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(XAAS_ROOT)/template/slim/extension
SLIMTEMPLATEDIR=$(XAAS_ROOT)/template/slim
SLIM_SERVICE_HOST=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(SLIMTEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/defaultvar.macros | grep SLIM_MACRO_SERVICE_HOST | awk '{print $$2}')
SLIM_BRIDGE2G_SENTINEL_PORT=$(shell cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(SLIMTEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/defaultvar.macros | grep SLIM_MACRO_BRIDGE2G_SENTINEL_HTTP_PORT | awk '{print $$2}')
	
_all: all

default: all

all: clean
	mkdir -p scope
	mkdir -p conf
	mkdir -p profile
	echo "extension"  > scope/extension.scope
	cp $(TEMPLATEDIR)/spec.template .
	cp $(TEMPLATEDIR)/conf/xdaqd.extension.conf.in conf/
	if [ -e hosts.setup.template ]; then \
		cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) hosts.setup.template hosts.setup; \
	else \
		cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -I$(XAAS_ROOT)/template -DSLIM_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/hosts.setup.template hosts.setup; \
	fi
	sed -i 's/@@/\/\//g' hosts.setup
	find $(BUILD_HOME)/$(Package)/profile/ -type f -name "*.configure.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename%.template}' \;
	find $(BUILD_HOME)/$(Package)/profile/ -type f -name "*.profile.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} profile/$${filename%.template}' \;
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.conf.template" -exec bash -c 'filename={}; filename=$$(basename $$filename); cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(XAAS_ROOT)/template -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DSLIM_MACRO_ZONE=$(ZONE_NAME) {} conf/$${filename%.template}' \;
	find $(BUILD_HOME)/$(Package)/profile/ -type f -name "*.configure" -exec sed -i "s/@@/\/\//g" {} \;
	find $(BUILD_HOME)/$(Package)/profile/ -type f -name "*.profile" -exec sed -i "s/@@/\/\//g" {} \;
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.conf" -exec sed -i "s/@@/\/\//g" {} \;
	mkdir -p system
	mkdir -p system-preset
	if [ -d $(BUILD_HOME)/$(Package)/system.template/ ]; then \
		find $(BUILD_HOME)/$(Package)/system.template/ -type f -name "*.template" -exec bash -c 'filename={}; filename=$$(basename -s .template $$filename); servicename=$${filename%.*}; servicetype=$${filename##*.}; cpp -P -I$(BUILD_HOME)/$(Package)/.. -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) -DXAAS_MACRO_UNIT_NAME=$${servicename} {} system/$(ZONE_NAME).$${servicename}@.$${servicetype};' \; ; \
	fi
	if [ -d $(BUILD_HOME)/$(Package)/system-preset.template/ ]; then \
		find $(BUILD_HOME)/$(Package)/system-preset.template/ -type f -name "*.template" -exec bash -c 'filename={}; filename=$$(basename -s .template $$filename); servicename=$${filename%.*}; cpp -P -I$(BUILD_HOME)/$(Package)/.. -I$(SLIMTEMPLATEDIR)/ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} system-preset/55-$(ZONE_NAME).extension.$${servicename}.preset;' \; ; \
	fi
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.conf" -exec grep -q XDAQ_CONFIG {} \; -exec bash -c 'filename={}; filename=$$(basename $$filename); unit=$${filename%.conf}; cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(SLIMTEMPLATEDIR)/ -DXAAS_MACRO_UNIT_NAME=$${unit}  -DXAAS_MACRO_ENVIRNOMENTFILE=$${unit}".env" -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(SLIMTEMPLATEDIR)/extension/system/serviceIC.template system/$(ZONE_NAME).$${unit}@.service;' \;
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.conf" -not -exec grep -q XDAQ_CONFIG {} \; -exec bash -c 'filename={}; filename=$$(basename $$filename); unit=$${filename%.conf}; cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(SLIMTEMPLATEDIR)/ -DXAAS_MACRO_UNIT_NAME=$${unit}  -DXAAS_MACRO_ENVIRNOMENTFILE=$${unit}".env" -DXAAS_MACRO_ZONE=$(ZONE_NAME) $(SLIMTEMPLATEDIR)/extension/system/serviceI.template system/$(ZONE_NAME).$${unit}@.service;' \;
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.conf" -exec bash -c 'export XDAQ_ZONE=$(ZONE_NAME); filename={}; filename=$$(basename $$filename); unit=$${filename%.conf}; export CONFIGURATION=$${unit}; set -a; source conf/$${filename}; set +a; envsubst < conf/$${filename} > conf/$${unit}.env' \;
	find $(BUILD_HOME)/$(Package)/conf/ -type f -name "*.env" -exec sed -i "s/@@/\/\//g" {} \;
	$(XAAS_ROOT)/template/scripts/instantiateTargets.awk "templatedir=$(SLIMTEMPLATEDIR)/extension/system" "targetdir=system" "zone=$(ZONE_NAME)" "scope=extension" hosts.setup
	find system -type f -name "*.target.cpp" -exec bash -c 'filename={}; filename=$$(basename $$filename);  cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(SLIMTEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME) {} system/$${filename%.cpp}' \;
	cpp -P -I$(BUILD_HOME)/$(Package)/../ -I$(TEMPLATEDIR)/ -DXAAS_MACRO_ZONE=$(ZONE_NAME)  $(SLIMTEMPLATEDIR)/extension/system-preset/preset.template system-preset/50-$(ZONE_NAME).extension.preset
	find $(BUILD_HOME)/$(Package)/system/ -type f -name "*.target" -exec sed -i "s/@@/\/\//g" {} \;
	find $(BUILD_HOME)/$(Package)/system/ -type f -name "*.service" -exec sed -i "s/@@/\/\//g" {} \;
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	$(XAAS_ROOT)/template/scripts/instantiateXML.awk "argument=conf/xdaqd.extension.conf.in" hosts.setup

_cleanall: clean

clean:
	-rm -f conf/xdaqd.*.extension.conf
	-rm -f conf/xdaqd.extension.conf.in
	-rm -f hosts.setup
	-rm -rf scope
	-rm -f spec.template 
	-rm -rf cron.d
	-rm -rf conf/*.conf
	-rm -rf conf/*.env
	-rm -rf profile/*.profile
	-rm -rf profile/*.configure
	-rm -rf system
	-rm -rf system-preset	


include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
