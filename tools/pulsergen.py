import sys
import os
import io
import json
from xml.dom import Node, minidom

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 5:
	print("Usage python3 pulsergen.py <zone> <tag> <inputDir> <outputDir>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]
inputDir = sys.argv[3]
outputDir = sys.argv[4]

print("zone = '{0}', tag = '{1}', inputDir = '{2}', outputDir = '{3}'".format(zone, tag, inputDir, outputDir))

#
# convert original XML flashlist in json format (in elasticsearch _meta )
#
def generateJSONPulsers(zone, tag, name, domFlashlist):
	root = domFlashlist.documentElement;
	events = []
	for event in root.childNodes:
		if (event.nodeType == Node.ELEMENT_NODE) and (event.localName == "event"):
			e = {}
			for attribName in event.attributes.keys():
				attribValue = event.getAttribute(attribName)
				e[attribName] = attribValue
			fs = []
			for flashlist in event.childNodes:
				if (flashlist.nodeType == Node.ELEMENT_NODE) and (flashlist.localName == "flash"):
					f = {}
					attribValue = flashlist.getAttributeNS("http://www.w3.org/1999/xlink", "href")
					f["name"] = attribValue.split("#")[1]
					f["tag"] = flashlist.getAttribute("tag")
					fs.append(f)
			e["flashlists"] = fs
			events.append(e)
	json = {"events": events, "service" : name}
	return json

def crawlDirectories():
	for filename in os.listdir(inputDir):
		if filename.endswith(".pulser"):
			name = os.path.splitext(filename)[0]
			filePath = os.path.join(inputDir, filename)
			domFlashlist = minidom.parse(filePath)
			#print(filePath)
			#with open(filePath, 'r') as fin:
			#	print(fin.read())
			document = generateJSONPulsers(zone, tag, name,  domFlashlist)
			outputfilepath = os.path.join(outputDir, name + ".json")
			with open(outputfilepath, 'w') as outfile:
				json.dump(document, outfile, indent = 3, separators = (',', ': '))
			print("Generated file: {0}".format(outputfilepath))
			#print(json.dumps(document, indent = 3, separators = (',', ': ')))

crawlDirectories()

