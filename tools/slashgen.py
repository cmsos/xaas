import sys
import os
import io
import json
import re
from xml.dom import Node, minidom

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 5:
	print("Usage python3 settingsgen.py <zone> <tag> <inputDir> <outputDir>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]
inputDir = sys.argv[3]
outputDir = sys.argv[4]

print("zone = '{0}', tag = '{1}', inputDir = '{2}', outputDir = '{3}'".format(zone, tag, inputDir, outputDir))

#
# convert original XML collector settings in json format
#
def generateJSONSlashSettings(zone, tag, slashInstance, domSlashSettings):
	root = domSlashSettings.documentElement
	for flashlist in root.childNodes:
		if (flashlist.nodeType == Node.ELEMENT_NODE) and (flashlist.localName == "flash"):
			f = {}
			for attribName in flashlist.attributes.keys():
				attribValue = flashlist.getAttribute(attribName)
				f[attribName] = attribValue
			f["slashInstance"] = slashInstance
			outputfilepath = os.path.join(outputDir, f["name"] + "-" + slashInstance  + ".json")
			with open(outputfilepath, 'w') as outfile:
				json.dump(f, outfile, indent = 3, separators = (',', ': '))
			print("Generated file: {0}".format(outputfilepath))

def crawlDirectories():
	for filename in os.listdir(inputDir):
		m = re.search('^(.*)\.settings$', filename)
		if m:
			slashInstance = m.group(1);
			filePath = os.path.join(inputDir, filename)
			print("filePath = {0}, slashInstance = {1}".format(filePath, slashInstance))
			domSlashSettings = minidom.parse(filePath)
			generateJSONSlashSettings(zone, tag, slashInstance, domSlashSettings)

crawlDirectories()
