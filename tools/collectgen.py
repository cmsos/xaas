import sys
import os
import io
import json
from xml.dom import Node, minidom

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 5:
	print("Usage python3 collectgen.py <zone> <tag> <inputDir> <outputDir>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]
inputDir = sys.argv[3]
outputDir = sys.argv[4]

print("zone = '{0}', tag = '{1}', inputDir = '{2}', outputDir = '{3}'".format(zone, tag, inputDir, outputDir))

#
# convert original XML collector settings in json format
#
def generateJSONCollectorSettings(zone, tag, domCollectorSettings):
	root = domCollectorSettings.documentElement
	for flashlist in root.childNodes:
		f = {}
		if (flashlist.nodeType == Node.ELEMENT_NODE) and (flashlist.localName == "flashlist"):
			qname = flashlist.getAttribute("name")
			fname = qname.split(":")[2]
			#print("qname = {0}".format(qname))
			f["name"] = qname
			c = {}
			for collector in flashlist.childNodes:
				if (collector.nodeType == Node.ELEMENT_NODE) and (collector.localName == "collector"):
					for attribName in collector.attributes.keys():
						attribValue = collector.getAttribute(attribName)
						c[attribName] = attribValue
						#print("{0}: {1}".format(attribName, attribValue))
						attributeNode = collector.getAttributeNode(attribName)
						if attributeNode.prefix != None:
							c["xmlns:" + attributeNode.prefix] = attributeNode.namespaceURI
				f["collector"] = c
			outputfilepath = os.path.join(outputDir, fname + ".json")
			with open(outputfilepath, 'w') as outfile:
				json.dump(f, outfile, indent = 3, separators = (',', ': '))
			print("Generated file: {0}".format(outputfilepath))
			#print(json.dumps(f, indent = 3, separators = (',', ': ')))

def crawlDirectories():
	for filename in os.listdir(inputDir):
		#print("filename = {0}".format(filename))
		if filename == "collector.settings":
			name = os.path.splitext(filename)[0]
			filePath = os.path.join(inputDir, filename)
			domCollectorSettings = minidom.parse(filePath)
			#print(filePath)
			#with open(filePath, 'r') as fin:
			#	print(fin.read())
			generateJSONCollectorSettings(zone, tag, domCollectorSettings)

crawlDirectories()
