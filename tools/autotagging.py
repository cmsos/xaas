import sys
import http.client
import json
import datetime
from constants import *

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 5:
	print("Usage python3 deploy.py <elasticsearch url> <zone> <tag> <autotagtype>")
	exit()

elasticUrl = sys.argv[1]
zone = sys.argv[2]
tag = sys.argv[3]
autotagtype = sys.argv[4]

print("elasticUrl = '{0}', zone = '{1}', tag = '{2}', autotagtype = '{3}'".format(elasticUrl, zone, tag, autotagtype))

connection = http.client.HTTPConnection(elasticUrl)
headers = {'Content-type': 'application/json'}
indexname = "cmsos-meta-" + zone + "-tags-registry"

connection.request('HEAD', indexname)
response = connection.getresponse()
result = response.read().decode()
if (response.status != 200):
	#Creating index
	print("Creating a new index " + indexname)
	connection.request('PUT', indexname + "?pretty")
	response = connection.getresponse()
	result = response.read().decode()
	print(result)
	print

	#Adding mapping
	print("Adding a mapping to the index " + indexname)
	document = {"_meta": {"zone": zone, "signature": signature}, "properties": {"timestamp": {"type": "date", "format" : "yyyy-MM-dd HH:mm:ss"}, "tag": {"type": "keyword"}, "autotagtype": {"type": "keyword"}}}
	connection.request('PUT', indexname + "/_mapping/_doc?pretty", json.dumps(document), headers)
	response = connection.getresponse()
	result = response.read().decode()
	print(result)
	print

#Adding a tag
print("Adding a tag '" + tag + "'")
now = datetime.datetime.utcnow()
document = {"timestamp": now.strftime("%Y-%m-%d %H:%M:%S"), "tag": tag, "autotagtype": autotagtype}
connection.request('POST', indexname + "/_doc?pretty", json.dumps(document), headers)
response = connection.getresponse()
result = response.read().decode()
print(result)
print
